//
//  TalksViewController.swift
//  Talkabit
//
//  Created by Rene Ramirez on 9/30/15.
//  Copyright © 2015 Rene Ramirez. All rights reserved.
//

import UIKit

class TalksViewController: UITableViewController {
    var room:Room!
    var talks:[Talk] = [Talk]()
    var talk: Talk!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
      talks = room.talks
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

  

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
      
        return talks.count
    }

  
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("talkCell", forIndexPath: indexPath)
        let talk = talks[indexPath.row] as Talk;
        let imageView = cell.viewWithTag(201) as! UIImageView
        imageView.image = UIImage(named: talk.profileImage!)
        let imagePreviewView = cell.viewWithTag(202) as! UIImageView
        let imagePreviewName:String = String(talk.imagePreview!) + ".jpeg"
        imagePreviewView.image = UIImage(named: imagePreviewName)
        let durationLabel = cell.viewWithTag(203) as! UILabel
        durationLabel.text = talk.duration
      
      
        return cell
    }
  

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      if(segue.identifier == "talkSegue"){
        let indexPath = self.tableView.indexPathForSelectedRow!
        talk = talks[indexPath.row] as Talk
        let viewController:TalkViewController = segue.destinationViewController as! TalkViewController
        viewController.talk = talk
      }

        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
  

}
