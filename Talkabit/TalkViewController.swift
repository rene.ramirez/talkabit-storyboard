//
//  TalkViewController.swift
//  Talkabit
//
//  Created by Rene Ramirez on 10/1/15.
//  Copyright © 2015 Rene Ramirez. All rights reserved.
//

import UIKit

class TalkViewController: UIViewController {
    var talk: Talk!
  @IBOutlet weak var durationLabel: UILabel!
  @IBOutlet weak var previewImage: UIImageView!
  @IBOutlet weak var profileImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(talk);
        // Do any additional setup after loading the view.
    }

  override func viewWillAppear(animated: Bool) {
    durationLabel.text = talk.duration
    let imagePreviewName:String = String(talk.imagePreview!) + ".jpeg"
    previewImage.image = UIImage(named: imagePreviewName)
    profileImage.image = UIImage(named: talk.profileImage!)
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}