//
//  RoomsViewController.swift
//  Talkabit
//
//  Created by Rene Ramirez on 9/30/15.
//  Copyright © 2015 Rene Ramirez. All rights reserved.
//

import UIKit

class RoomsViewController: UITableViewController {
    var rooms:[Room] = roomsData
    var room:Room!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return rooms.count
    }

  
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("roomCell", forIndexPath: indexPath)

      let room = rooms[indexPath.row] as Room
      if let nameLabel = cell.viewWithTag(100) as? UILabel { //3
        nameLabel.text = room.name
      }
      if let numberViewsLabel = cell.viewWithTag(101) as? UILabel { //3
        numberViewsLabel.text = room.numberViews
      }
      if let numberVideosLabel = cell.viewWithTag(102) as? UILabel { //3
        numberVideosLabel.text = room.numberVideos
      }
     

        return cell
    }
  

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
      if(segue.identifier == "talksSegue"){
        let indexPath = self.tableView.indexPathForSelectedRow!
        room = rooms[indexPath.row] as Room
        let viewController:TalksViewController = segue.destinationViewController as! TalksViewController
        viewController.room = room;
      }
    }
  

}
