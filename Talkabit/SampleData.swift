//
//  SampleData.swift
//  Talkabit
//
//  Created by Rene Ramirez on 9/30/15.
//  Copyright © 2015 Rene Ramirez. All rights reserved.
//

let roomsData = [
   Room(
    name: "#Webd",
    numberViews: "104 k",
    talks:
    [
      Talk(
        name:"a",
        profileImage: "profile-001",
        imagePreview: "preview-001",
        duration: "18 sec"
        ),
      Talk(
        name:"a",
        profileImage: "profile-002",
        imagePreview: "preview-002",
        duration: "30 sec"
      )

      ]
    ),
   Room(
    name: "#Toronto",
    numberViews: "15 k",
    talks:
    [
      Talk(
        name:"a",
        profileImage: "profile-002",
        imagePreview: "preview-003",
        duration: "18 sec"
      ),
      Talk(
        name:"a",
        profileImage: "profile-003",
        imagePreview: "preview-004",
        duration: "8 sec"
      ),
      Talk(
        name:"a",
        profileImage: "profile-001",
        imagePreview: "preview-005",
        duration: "48 sec"
      ),
      Talk(
        name:"a",
        profileImage: "profile-004",
        imagePreview: "preview-006",
        duration: "33 sec"
      )
    ]
  ),
   Room(
    name: "#Humber",
    numberViews: "500",
    talks:
    [
      
      Talk(
        name:"a",
        profileImage: "profile-003",
        imagePreview: "preview-001",
        duration: "18 sec"
      ),
      Talk(
        name:"a",
        profileImage: "profile-001",
        imagePreview: "preview-002",
        duration: "8 sec"
      ),
      Talk(
        name:"a",
        profileImage: "profile-003",
        imagePreview: "preview-003",
        duration: "48 sec"
      )

    ]
    ),
   Room(
    name: "#College",
    numberViews: "15 k",
    talks:
    [
      Talk(
        name:"a",
        profileImage: "profile-003",
        imagePreview: "preview-003",
        duration: "48 sec"
      )
    ]
  ),
   Room(
    name: "#Spanish",
    numberViews: "64 k",
    talks:
    [
      Talk(
        name:"a",
        profileImage: "profile-004",
        imagePreview: "preview-004",
        duration: "48 sec"
      )
    ]
    ),
   Room(
    
    name: "#Xcode",
    numberViews: "14 k",
    talks:
    [
      Talk(
        name:"a",
        profileImage: "profile-001",
        imagePreview: "preview-005",
        duration: "48 sec"
      )
    ]
    ),
   Room(
    name: "#Sturf",
    numberViews: "18 k",
    talks:
    [
      Talk(
        name:"a",
        profileImage: "profile-002",
        imagePreview: "preview-006",
        duration: "48 sec"
      )
    ]
    )

]