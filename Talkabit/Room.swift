//
//  Room.swift
//  Talkabit
//
//  Created by Rene Ramirez on 9/30/15.
//  Copyright © 2015 Rene Ramirez. All rights reserved.
//

import UIKit

struct Room {
  var name: String?
  var numberViews: String?
  var numberVideos: String?
  var talks:[Talk]
  init(name: String?, numberViews: String?, talks:[Talk]){
    self.name = name 
    self.numberViews = numberViews! + "Views"
    self.numberVideos = String(talks.count) + " Videos"
    self.talks = talks
  }
}
