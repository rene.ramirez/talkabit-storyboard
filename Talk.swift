//
//  Talk.swift
//  Talkabit
//
//  Created by Rene Ramirez on 9/30/15.
//  Copyright © 2015 Rene Ramirez. All rights reserved.
//

import UIKit

struct Talk {
  var name: String?
  var profileImage: String?
  var imagePreview: String?
  var duration: String?
  init(name: String?,profileImage: String?, imagePreview: String?, duration: String?){
    self.name = name
    self.profileImage = profileImage
    self.imagePreview = imagePreview
    self.duration = duration
  }
}
